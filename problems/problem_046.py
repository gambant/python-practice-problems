# Complete the make_sentences function that accepts three
# lists.
#   * subjects contains a list of subjects for three-word sentences
#   * verbs contains a list of verbs for three-word sentences
#   * objects contains a list of objects for three-word sentences
# The make_sentences function should return all possible sentences
# that can be made from the words in each list
#
# Examples:
#   * subjects: ["I"]
#     verbs:    ["play"]
#     objects:  ["Portal"]
#     returns:  ["I play Portal"]
#   * subjects: ["I", "You"]
#     verbs:    ["play"]
#     objects:  ["Portal", "Sable"]
#     returns:  ["I play Portal", "I play Sable",
#                "You play Portal", "You play Sable"]
#   * subjects: ["I", "You"]
#     verbs:    ["play", "watch"]
#     objects:  ["Portal", "Sable"]
#     returns:  ["I play Portal", "I play Sable",
#                "I watch Portal", "I watch Sable",
#                "You play Portal", "You play Sable"
#                "You watch Portal", "You watch Sable"]
#
# Do it without pseudocode, this time, from memory. Don't look
# at the last one you just wrote unless you really must.

def make_sentences(subjects, verbs, objects):
    # paramater_list = [subjects, verbs, objects]  #initialize a literal list using parameters
    # result_list = []
    # for list in paramater_list: #use a nested list to loop through each element of my paramaters
    #     result_list.append(list[0])
    # return result_list
    sentence = []
#   # subject verb object
    for i in range(len(subjects)):
        for j in range(len(verbs)):
            for k in range(len(objects)):
                mini_sentence = f"{subjects[i]} {verbs[j]} {objects[k]}"
                sentence.append(mini_sentence)
    return sentence


# sentence.append(subjects[range(len(subjects))] + verbs[range(len(verbs))] + objects[range(len(objects))])

subjects = ["I", "You"]
verbs = ["play"]
objects = ["Portal", "Sable"]

print(make_sentences(subjects, verbs, objects))

# def make_sentences(subjects, verbs, objects):
#     # subject verb object
#     sentence = []
#     mini_sentence = []
#     for i in subjects:
#         mini_sentence.append(i)
#         for j in verbs:
#             mini_sentence.append(i)
#             for k in objects:
#                 mini_sentence.append(i)
#     return mini_sentence

# # sentence.append(subjects[range(len(subjects))] + verbs[range(len(verbs))] + objects[range(len(objects))])

# subjects = ["I", "You"]
# verbs = ["play"]
# objects = ["Portal", "Sable"]

# print(make_sentences(subjects, verbs, objects))
# if they're all the same length
# for i in range(len(subjects)):
# result.append(subjects[i] + verbs[i] + objects[i])
def make_sentences(subjects, verbs, objects):
    result = []
    for subject in subjects:
        for verb in verbs:
            for object in objects:
                # print(f"{subject} {verb} {object}")
                result.append(subject + " " + verb + " " + object)
    return result

make_sentences(["I", "You"], ["play", "watch"], ["Portal", "Sable"])
