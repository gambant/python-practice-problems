# Write a function that meets these requirements.
#
# Name:       halve_the_list
# Parameters: a single list
# Returns:    two lists, each containing half of the original list
#             if the original list has an odd number of items, then
#             the extra item is in the first list
#
# Examples:
#    * input: [1, 2, 3, 4]
#      result: [1, 2], [3, 4]
#    * input: [1, 2, 3]
#      result: [1, 2], [3]

def halve_the_list(single_list):
        half = len(single_list) // 2    # // rounding down "flooring it" removing the decimal point
        if len(single_list) % 2 == 0:
            first_half = single_list[0 : half]
            second_half = single_list[half : len(single_list)]
        else:
              first_half = single_list[0 : half + 1]
              second_half = single_list[half + 1]
        return (first_half, second_half)

# import math
# half = math.ceil(len(single_list) / 2)  #math.ceil rounds up

# def halve_the_list(single_list):
#     list1 = []
#     list2 = []
#     middle_point = len(single_list) // 2
#     if len(single_list) % 2 == 0:
#         list1 = single_list[:middle_point]
#         list2 = single_list[middle_point:]
#     else:
#         list1 = single_list[:middle_point + 1]
#         list2 = single_list[middle_point + 1:]

#     return list1, list2
# single_list = [1, 2, 3, 4, 5]

# print(halve_the_list(single_list))

# def halve_the_list(single_list):
#         return (                                            # solution
#         input[0:len(input) // 2 + (len(input) % 2)],    # solution
#         input[len(input) // 2 + (len(input) % 2):],     # solution
#     )
input = [1, 2, 3, 4, 5]

print(halve_the_list(input))
