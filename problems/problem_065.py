# Write a function that meets these requirements.
#
# Name:       biggest_gap
# Parameters: a list of numbers that has at least
#             two numbers in it
# Returns:    the largest gap between any two
#             consecutive numbers in the list
#             (this will always be a positive number)
#
# Examples:
#     * input:  [1, 3, 5, 7]
#       result: 2 because they all have the same gap
#     * input:  [1, 11, 9, 20, 0]
#       result: 20 because from 20 to 0 is the biggest gap
#     * input:  [1, 3, 100, 103, 106]
#       result: 97 because from 3 to 100 is the biggest gap
#
# You may want to look at the built-in "abs" function

def biggest_gap(num_list):
    result = 0
    for num in range(0, (len(num_list) - 1)):
        if result < abs(num_list[num] - num_list[num + 1]):
            result = abs(num_list[num] - num_list[num + 1])

    return result

print(biggest_gap([1, 3, 100, 103, 106]))

# def biggest_gap(nums):                          # solution
#     max_gap = abs(nums[1] - nums[0])            # solution
#     for i in range(1, len(nums) - 1):           # solution
#         gap = abs(nums[i + 1] - nums[i])        # solution
#         if gap > max_gap:                       # solution
#             max_gap = gap                       # solution
#     return max_gap                              # solution

def biggest_gap(num_list):
    i = 0
    j = 1
    largest_gap = 0
    while j < len(num_list):
        difference = num_list[j] - num_list[i]
        if difference > largest_gap:
            largest_gap = difference
        i += 1
        j += 1
    return largest_gap
print(biggest_gap([1, 3, 100, 103, 106]))
