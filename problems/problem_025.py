# Complete the calculate_sum function which accepts
# a list of numerical values and returns the sum of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#

def calculate_sum(values):
    if len(values) == False:
        return None
    else:
        # return sum(values)
        total = 0
        for sum in values:
            total += sum
    return total

print(calculate_sum([2, 4, 5, 7]))
