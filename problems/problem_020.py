# Complete the has_quorum function to test to see if the number
# of the people in the attendees list is greater than or equal
# to 50% of the number of people in the members list.

def has_quorum(attendees_list, members_list):
    numattend = len(attendees_list)
    membattend = len(members_list)
    if numattend >= membattend / 2:
        return True
    else:
        return False
print(has_quorum(["john"], ["peter", "tommy", "eric", "asga", "agaag"]))
