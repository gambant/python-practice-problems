# Complete the calculate_average function which accepts
# a list of numerical values and returns the average of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
# Pseudocode is available for you

def calculate_average(values):
    if len(values) == 0:
        return None
    #will grab nums out of random_list with a for statement and add to values, then divide by the len of the list
    else:
        return sum(values) / len(values)
    #     total = 0

    #     for nums in values:
    #         total += nums
    #     avg = total / len(values)
    # return avg

    # values = values/divisor




print(calculate_average([1, 2, 3, 4]))
