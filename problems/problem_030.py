# Complete the find_second_largest function which accepts
# a list of numerical values and returns the second largest
# in the list
#
# If the list of values is empty, the function should
# return None
#
# If the list of values has only one value, the function
# should return None
#
# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def find_second_largest(values):
    largest = 0
    second_largest = 0
    if len(values) == 0 or len(values) == 1:
        return None
    else:
        for item in values:
            if item > largest:
                largest = item
            # return largest
        for item in values:
            if item > second_largest and item != largest:
                second_largest = item
        return second_largest

num_list = [1, 3, 4, 9, 30]

print(find_second_largest(num_list))
